"""


#!/usr/bin/env bash



myvm="fbno-poc_default_1509109446069_62467"


ovftool="/Applications/VMware\ OVF\ Tool/ovftool"


# chack that the vm is off ?

# export vm
#echo "export virtualbox to ova"
#VBoxManage export ${myvm} -o ./tmp/${myvm}.ova


echo "Apply vmware ovftool on the image"
/Applications/VMware\ OVF\ Tool/ovftool  --lax ./tmp/${myvm}.ova ./tmp/${myvm}.ovf

"""

import xml.etree.ElementTree as ET

import logging




OVFTOOL = '/Applications/VMware\ OVF\ Tool/ovftool'
VM_DIRECTORY = './tmp'

VM_SOURCE = ''
VM_TARGET = ''


SOURCE_HARDWARE_SATA = """\
    <Item>
    <rasd:Address>0</rasd:Address>
    <rasd:Caption>sataController0</rasd:Caption>
    <rasd:Description>SATA Controller</rasd:Description>
    <rasd:ElementName>sataController0</rasd:ElementName>
    <rasd:InstanceID>5</rasd:InstanceID>
    <rasd:ResourceSubType>AHCI</rasd:ResourceSubType>
    <rasd:ResourceType>20</rasd:ResourceType>
    </Item>
"""


TARGET_HARDWARE_SCSI = """\
    <Item>
    <rasd:Address>0</rasd:Address>
    <rasd:Caption>SCSIController</rasd:Caption>
    <rasd:Description>SCSI Controller</rasd:Description>
    <rasd:ElementName>SCSIController</rasd:ElementName>
    <rasd:InstanceID>5</rasd:InstanceID>
    <rasd:ResourceSubType>lsilogic</rasd:ResourceSubType>
    <rasd:ResourceType>6</rasd:ResourceType>
    </Item>
"""

namespaces= {

'xmlns':     "http://schemas.dmtf.org/ovf/envelope/1",
'ovf': "http://schemas.dmtf.org/ovf/envelope/1",
'rasd': "http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_ResourceAllocationSettingData",
'vbox': "http://www.virtualbox.org/ovf/machine" ,
'vmw': "http://www.vmware.com/schema/ovf" ,
'vssd': "http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_VirtualSystemSettingData",
'xsi': "http://www.w3.org/2001/XMLSchema-instance"
}


target_parameters= {

    'VirtualSystemType' : "vmx-10"
}


class VirtualHardwareSectionItem(ET.Element):
    """

        represent an item of VirtualHardwareSection/Item

    """
    children_tags= [
        "InstanceID","ElementName","ResourceType","ResourceSubType","Description","Caption","Address",
        "AddressOnParent","Parent","Connection"
    ]

    def __init__(self, source_element):
        """

        :param source_element: instance of ET.Element
        """
        self.tag = source_element.tag
        self.attrib = source_element.attrib
        self._children = source_element._children


    # def get_description(self):
    #     """
    #
    #     :return:
    #     """
    #     desc= self.find("rasd:Description",namespaces)
    #     if desc is not None:
    #         return desc.text
    #     else:
    #         logging.debug("cannot find item description")
    #         return None

    def get_child_text(self,child_tag):
        """
            return the text of a child s item

        :param child_tag: string ( one of ElementName, Description, ResourceType, InstanceID
        :return:
        """

        e= self.find("rasd:%s" % child_tag,namespaces)
        if e is not None:
            return e.text
        else:
            logging.debug("cannot find item tag: %s" % child_tag)
            return None



class Ovf(object):
    """
        an ovf file for a vmware machine

    """
    def __init__(self,xml_file):
        """

        :param xml_string:
        """
        self.tree = ET.parse(xml_file)
        self.root = self.tree.getroot()
        return

    def iter_VirtualHardwareSection_Item(self):
        """
            search for tag VirtualHardwareSection and iter on each <Item> tag

        :return:
        """
        vs = self.root.find(".//ovf:VirtualHardwareSection" ,namespaces)
        if vs is None:
            logging.error("cannot find tag VirtualHardwareSection")
            raise GeneratorExit


        for item in vs.findall("ovf:Item",namespaces):
            # found an item
            item = VirtualHardwareSectionItem(item)
            description = item.get_child_text("ElementName")
            logging.debug("found VirtualHardwareSection Item: [%s]" % description)
            print item.tag, item.attrib
            yield item



    def virtualSystemType(self):
        """

            return the following element:

            VirtualSystem/VirtualHardwareSection/System  <vssd:VirtualSystemType>vmx-07</vssd:VirtualSystemType>

        :return:
        """
        e= self.root.find('.//vssd:VirtualSystemType', namespaces)
        if e is not None:
            logging.debug("found tag %s : %s" % ("vssd:VirtualSystemType",e.text))
            return e
        else:
            logging.debug("cannot find tag %s" % "vssd:VirtualSystemType")
            return None

    def set_virtualSystemType(self,text):
        """

        :param text:
        :return:
        """
        e = self.virtualSystemType()
        if e is not None:
            logging.info("change virtualSystemType from %s to %s" % (e.text,text))
            e.text= text
            return True
        else:
            logging.error("cannot change virtualSystemType" )
            return False

    def dump(self, outfile="out.ovf"):
        """

        :return:
        """
        # debugging
        with open(outfile,"wb") as fw:
            self.tree.write(fw)
            tail = self.tree.getroot().tail
            if not tail or tail[-1] != "\n":
                fw.write("\n")
        return



if __name__=="__main__":

    logging.basicConfig(level=logging.DEBUG)

    filename= "./tmp/fbno-poc_default_1509109446069_62467.ovf"
    ovf = Ovf(filename)


    # get system type ( virtualbbox* , vmx* )
    #s= ovf.virtualSystemType()
    # set system type to vmx-10
    r = ovf.set_virtualSystemType(target_parameters["VirtualSystemType"])
    if r:
        s = ovf.virtualSystemType()
        assert s.text == target_parameters["VirtualSystemType"]
    else:
        raise RuntimeError("cannot change system type")




    items = ovf.iter_VirtualHardwareSection_Item()
    for item in items:
        for e in item:
            print e.tag,e.text




    ovf.dump()





    print("Done")