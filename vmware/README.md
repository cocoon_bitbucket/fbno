
# convert virtualbox to vmware

( not completed )


ref: http://cbalfour.github.io/2016-08-17-moving-vms-from-virtualbox-to-vmware-esxi/


# prerequisites

* virtulabox
    VboxManage

* vmware tools
  ofvtool
  install it: https://my.vmware.com/fr/web/vmware/details?productId=614&downloadGroup=OVFTOOL420

* a virtual box image to convert


on my mac
---------

vmware ovftool is at 

    /Applications/VMware\ OVF\ Tool/ovftool
    

virtual box images are generated in directory

    Users/cocoon/VirtualBox\ VMs/
    
    
# use converter

generate files

./convert.sh


edit the file ./tmp/${vm}.ovf


change
 
     <vssd:VirtualSystemType>virtualbox-2.2</vssd:VirtualSystemType>

to

    <vssd:VirtualSystemType>vmx-10</vssd:VirtualSystemType>
    
    
change 

    <Item>
    <rasd:Address>0</rasd:Address>
    <rasd:Caption>sataController0</rasd:Caption>
    <rasd:Description>SATA Controller</rasd:Description>
    <rasd:ElementName>sataController0</rasd:ElementName>
    <rasd:InstanceID>5</rasd:InstanceID>
    <rasd:ResourceSubType>AHCI</rasd:ResourceSubType>
    <rasd:ResourceType>20</rasd:ResourceType>
    </Item>
    
to 

    <Item>
    <rasd:Address>0</rasd:Address>
    <rasd:Caption>SCSIController</rasd:Caption>
    <rasd:Description>SCSI Controller</rasd:Description>
    <rasd:ElementName>SCSIController</rasd:ElementName>
    <rasd:InstanceID>5</rasd:InstanceID>
    <rasd:ResourceSubType>lsilogic</rasd:ResourceSubType>
    <rasd:ResourceType>6</rasd:ResourceType>
    </Item>