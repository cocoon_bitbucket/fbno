# README #

restop setup for fbno platform demo


this repository is for setting fbno demo platform (both server and client) to run test on livebox



server fbno-poc
===============

## install this repository
    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/fbno.git fbno
    
    
## create the server

### server overview

this http server is a demo platform with 

* 1 powerswitch energenie at 192.168.1.30 named energenie
* 4 livebox named LB1 to LB4 at 192.168.1.1 to 192.168.1.4
* 1 liveboxconnector ( connected to LB1)
* 1 influxdb sever
* 1 grafana server

see configuration at

    fbno/fbno-poc/master/platform.yml

### server composition
It uses docker images on docker hub (most images are based on alpine)

* cocoon/restop-devices  (main test solution)
* appcelerator/grafana
* redis:alpine
* anapsix/alpine-java:7  ( for livebox connector)
* influxdb:alpine
* nginx:stable-alpine


see configuration at
    fbno/fbno-poc/docker-compose.yml


### server installation

    cd fbno/fbno-poc
    docker-compose up
    
The first time you launch it can take a while because all needed docker 
images will be pulled from dockerhub


### server usage

once the server is started you need a "restop client" to use it

see fbno/fbno-poc/client/README.md to install a restop client

test for fbno-poc demo platform are available at
     fbno/tests/client/basic.robot
     fbno/tests/client/basic.py
    

### server update

to install a new version of the server we need to stop the running server

    cd fbno/fbno-poc
    docker-compose down --rmi local
    
upgrade this repository

    git pull
    
install the new image of the main restop images
    
    docker pull cocoon/restop-devices
    
restart the server

    docker-compose up
    



   
## client for fbno-poc demo platform

this client contains a robotframework plugin so we can write
robotframework scripts to remotly test the platform


### client installation


    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/restop-client restop-client
    cd restop-client
    pip install -r requirements.txt
    python setup.py install
 
check installation is ok with

    pybot --help
    
 
 
### client usage

once a server is started at address $server_host

customize the robot framework test

    cd fbno/tests/client

edit the file fbno/tests/client/basic.robot to replace "192.168.1.21" with the $server_address in 
the following line
    
    ${platform_url}=  http://192.168.1.21/restop/api/v1


start the test

    pybot basic.robot
    
open your browser on the ressult file
   
    fbno/tests/client/log.html
   
   




    
    
    
    
    