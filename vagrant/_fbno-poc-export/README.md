a base vm to deploy to vmware



the making of
-------------
start with fbno/vagrant/fbno-poc-export/Vagrantfile

change network config to 

    config.vm.network "public_network", ip: "192.168.1.101" ,bridge: "eth0"


add the install-fbno-poc.sh



configuration
=============
public network at 192.168.1.101
RAM 8192
login: vagrant/vagrant
sudo password: vagrant


use it
======

vagrant ssh
cd fbno/fbno-poc
docker-compose up
