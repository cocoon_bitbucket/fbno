a vm to deploy fbno-poc



* based on debian jessie + docker + docker-compose
* install this repository on it
* build the images
  * cocoon/restop-devices
  * redis:alpine
  * ...
  
  

the making of
-------------
start with fbno/vagrant/devian-docker/Vagrantfile

add the install-fbno-poc.sh


configuration
=============
public network at 192.168.1.101
RAM 8192
login: vagrant/vagrant
sudo password: vagrant


use it
======

vagrant ssh
cd fbno/fbno-poc
docker-compose up
