
a base vm for docker on alpine
==============================

WORK IN PROGRESS

use debian/Vagrantfile instead


last problem:
-------------
livebox connector does not work ( memory problem on jvm )


vagrant box
===========

alpine vagrant box  
see: https://github.com/maier/vagrant-alpine


    vagrant plugin install vagrant-alpine
    
    cd fbno/vagrant/base
    vagrant init maier/alpine-3.2.3-x86_64
    
    
add lines to Vagrantfile

    config.vm.box_check_update = false
    config.vbguest.auto_update = false
    config.vm.synced_folder "../data", "/vagrant_data" , disabled: true
    

add community repository for docker

alpine34:~$ sudo vi /etc/apk/repositories

    http://dl-cdn.alpinelinux.org/alpine/v3.4/main
    http://dl-cdn.alpinelinux.org/alpine/v3.4/community


 install docker and docker-compose on alpine
 see: https://wiki.alpinelinux.org/wiki/Docker
 
    sudo apk update
    sudo apk add docker
    
To install docker-compose, first install pip:

    sudo apk add py-pip
    sudo pip install docker-compose 
    
    
To start the Docker daemon at boot, run:

    sudo rc-update add docker boot


Then to start the Docker daemon manually, run:

    sudo service docker start 
    
    
install fbno-poc

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/fbno.git fbno
    
    
