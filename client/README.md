install the restop client:
==========================

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/restop-client restop-client
    cd restop-client
    pip install -r requirements.txt
    python setup.py install
    
