__author__ = 'cocoon'
"""
    the master restop application


    urls

    restop/api/v1/  =>
            POST /restop/api/v1/sessions : create a new session
            POST /restop/api/v1/session/1/start : start the session
            POST /restop/api/v1/session/1/stop  : stop the session


    restop/hub/<hub_name>/* =>
        router to devices

        redirect to /restop/api/v1/<hub_name>/*



"""

from flask import request

from restop.application import get_application
from wbackend.model import Model


from restop.collection import GenericCollectionWithOperationApi
from restop_platform import Dashboard
from restop_platform.journal import Journal

from restop_queues.apps.queue_server import QueueServer

from router_blueprint import router
from uploader_blueprint import uploader


from restop_graph.influxdb import Publisher,Subscriber,Injector

from restop_components.powerswitch_device.powerswitch_starter import PowerswitchStarter


# import resources
import resources



#
# create flask app
#

# get a flask application
app = get_application(__name__,with_error_handler=True)

app.config.from_pyfile('config.cfg')

redis_url= app.config.get('RESTOP_REDIS_URL',"")

# backend= Backend.new(redis_url,
#                      workspace_base=app.config['WORKSPACE_ROOT'],
#                      spec_base= app.config["SPEC_ROOT"],
#                      config=app.config)


#
app.config['backend'] = Model

#
# create collection blueprint
#
blueprint_name='restop_master'
#url_prefix= '/restop/api/v1'

url_prefix= app.config['RESTOP_URL_PREFIX']

hub_url_prefix= app.config.get('RESTOP_HUB_URL_PREFIX','/restop/hub')
upload_url_prefix=app.config.get('RESTOP_UPLOAD_URL_PREFIX','/restop/upload')

collections= [ "root", "oauth2","files","platforms","sessions","apidoc"]


app.config['collections']= {}
app.config['collections'][blueprint_name]= collections


my_blueprint= GenericCollectionWithOperationApi.create_blueprint(blueprint_name,__name__,url_prefix=url_prefix,
                collections= collections)


# register hub routing blueprint
#app.register_blueprint(router,url_prefix=url_prefix+'/hub')
app.register_blueprint(router,url_prefix=hub_url_prefix)
app.register_blueprint(uploader,url_prefix=upload_url_prefix)

# register restop blueprint
app.register_blueprint(my_blueprint,url_prefix='')



#
#  urls
#

@app.route('/')
def index():
    """

    :return:
    """
    root_url=  "%s%s" % (request.base_url[:-1],app.config['RESTOP_URL_PREFIX'])
    return 'hello from restop server: try <a href="%s">%s</a>' % (root_url,root_url)


# @app.route('/restop/api/v1/hub/<collection>/<path:path>' ,methods=['GET', 'POST'])
# def hub_router(collection,path):
#     """
#
#     """
#
#     return "hub routing to collection %s" % collection




def start_server(**kwargs):
    """


    :param kwargs:
    :return:
    """
    app.run(**kwargs)




if __name__=="__main__":

    from yaml import load
    import time

    from flask.config import Config
    from restop_platform.models import *

    import logging
    logging.basicConfig(level=logging.DEBUG)

    print("Starting Master ...")

    root_path = dir_path = os.path.dirname(os.path.realpath(__file__))

    # get config from local config.cfg
    config = Config(root_path)
    config.from_pyfile('config.cfg')
    master_url = config['RESTOP_MASTER_URL']
    hub_name=    config['RESTOP_HUB_NAME']
    platform_file= config['RESTOP_PLATFORM']

    host= config.get('RESTOP_HOST','localhost')
    port= config.get('RESTOP_PORT',5000)


    # ...
    db = Database(
        host= config.get('RESTOP_REDIS_HOST' ,'localhost'), 
        port= config.get('RESTOP_REDIS_PORT',6379),
        db= config.get('RESTOP_REDIS_DB',0)
    )
    app.config['db'] = db
    app.config['journal']= Journal(db)
    Model.bind(database=db,namespace=None)
    app.config['journal'].write("start Master journal")


    # raz all databases
    if config.get('RESTOP_REDIS_FLUSHALL',False):
        db.flushall()

    try:
        p= Platform.get(Platform.name=='default')
    except Exception as e:
        # does not exists create it
        db.flushdb()
        #stream = file('../../tests/platform.yml')
        stream = file(platform_file)
        data = load(stream)
        print("create platform with data: --[\n%s\n]--" % data)
        p = Platform.create(name='default', data=data)
        p.setup()

        #
        # setup apidoc
        #
        # store api doc
        collection_operations = set()
        operations = set()


        api = Apidoc.create(platform_name='default',
                            session_collection='sessions',
                            collections=collections,
                            collection_operations= list(collection_operations),
                            operations=list(operations)
                            )


    p = Platform.get(Platform.name == 'default')

    # start usb hub server if serial_port in platform
    app.config['journal'].write('looking for serial ports')

    #
    # start the queue server  ( for usb queues )
    #
    qserver= QueueServer(app.config['QUEUES'],redis_db=db)
    qserver.start()

    #
    # start the influxdb Subscriber
    #
    injector = Injector("influxdb", dbname="graphite")

    subscriber = Subscriber(db)
    subscriber.set_worker(injector)
    subscriber.start()

    #
    #   start powerswitch devices
    #
    app.config['journal'].write('starting powerswitch servers')
    powerswitch= PowerswitchStarter("default")
    app.config['journal'].write('found powerswitch devices:%s' % str(powerswitch.devices))
    powerswitch.start()
    app.config['journal'].write('powerswitch servers started')


    app.config['journal'].write("start http server ")
    start_server(host='0.0.0.0', port=port, debug=False, threaded=True)
