grafana default account
-----------------------
User: admin
Password: password










grafana api
===========

see: http://simonjbeaumont.com/posts/docker-dashboard/


authenticate
------------
curl http://admin:password@192.168.99.100:3000/api/org



Create restop datasource
------------------------
POST /api/datasources HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "name":"restop",
  "type":"graphite",
  "url":"http://localhost:8000",
  "access":"proxy",
  "basicAuth":false
}

create dashboard
----------------

POST /api/dashboards/db HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "dashboard": {
    "id": null,
    "title": "restop",
    "tags": [ "restop" ],
    "timezone": "browser",
    "rows": [
      {
      }
    ],
    "schemaVersion": 6,
    "version": 0
  },
  "overwrite": false
}


configure grafana for dashboard
--------------------------------

[dashboards.json]
enabled = true
path = /var/lib/grafana/dashboards/