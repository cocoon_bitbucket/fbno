#!/usr/bin/env bash
# stop the server and delete images
docker-compose down --rmi local
# update the repository
git pull
# update the restop image
docker pull cocoon/restop-devices
# restart the server
docker-compose up
