__author__ = 'cocoon'

import time

import os
from restop_client import Pilot


phonehub_url= "http://localhost:/restop/api/v1"
#phonehub_url= "http://192.168.99.100/restop/api/v1"
#phonehub_url= "http://10.179.5.174/restop/api/v1"
#phonehub_url= "http://192.168.1.21/restop/api/v1"
#phonehub_url= "http://192.168.1.26/restop/api/v1"
#phonehub_url= "http://192.168.1.11/restop/api/v1"

# fbno-poc vm
#phonehub_url= "http://192.168.1.101/restop/api/v1"

# fbno-poc lannion 10.194.67.222
phonehub_url= "http://10.194.67.222:/restop/api/v1"


# livebox LB1 but not connected to PDU  ( to test pdu plug 4 )
user1 = 'LB4'

def test_fetch_restop_interface():
    """



    :return:
    """
    # to test localy
    #phonehub_url = "http://localhost:5001/restop/api/v1"
    #os.environ['NO_PROXY'] = 'localhost'

    p= Pilot()

    data= p.fetch_restop_interface(url=phonehub_url)

    return data





#
#  dummy tvboxrunner
#


def test_basic():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session(user1)
        time.sleep(2)

        p.power_on(user1)
        #p.send(user1, cmd=u'echo "hello world"')
        time.sleep(3)

        outlets= p.power_status(user1)
        print("power status: %s" % outlets)


        #res = p.watch(user1)
        p.power_off(user1)


    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return


def test_boxconnector():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session(user1)
        time.sleep(2)

        rc= p.PrintInfo(user1)

        rc= p.GetProperty(user1,name='TV_Status')

        time.sleep(3)

    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return


def test_boxconnector_endurance(max):

    p =Pilot()

    for i in range(1,max):
        p.setup_pilot("demo", "demo_qualif", phonehub_url)
        print("====================================================   iteration= %d" % i)
        try:

            res= p.open_Session(user1)
            time.sleep(2)

            rc= p.PrintInfo(user1)
            assert "Version" in rc.keys(), "PrintInfo failed"
            rc= p.GetProperty(user1,name='TV_Status')
            assert "TV_Status" in rc.keys(), "GetProperty failed"
            time.sleep(3)

        except RuntimeError as e:
            print("interrupted")
            raise RuntimeError("Failed message:%s" % e.message)

        except Exception as e:
            print(e.message)
            raise RuntimeError("Failed message:%s" % e.message)
        finally:
            r= p.close_session()

    return





def test_scheduler():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)

    try:

        res= p.open_Session(user1)

        res = p.sync(user1)

        # start scheduler
        res= p.scheduler_init(user1, metrics=["df", "meminfo", "cpu","loadavg" ])
        #res = p.init_metrics(user1,metrics=["df", "meminfo", "cpu","loadavg" ])

        p.publish_event(user1, title="init_scheduler")

        res = p.watch(user1, timeout=6)

        # collect stats
        #res= p.scheduler_sync(user1)
        time.sleep(60)
        #res = p.telnet_watch(user1, timeout=6)
        p.publish_event(user1, title="end of work")
        # close scheduler
        #res= p.scheduler_close(user1)
        #time.sleep(15)
        res= p.watch(user1,timeout=10)


    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()


    return


def test_event_publisher():

    p =Pilot()

    p.setup_pilot("demo", "demo_qualif", phonehub_url)


    try:

        res= p.open_Session(user1)
        time.sleep(2)


        p.publish_event(user1, title="my_open_event")

        time.sleep(5)

        p.publish_event(user1,title="my_close_event")



    except RuntimeError as e:
        print("interrupted")

    except Exception as e:
        print(e.message)
    finally:
        r= p.close_session()

    return






if __name__=="__main__":


    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_fetch_restop_interface()
    test_basic()
    #test_boxconnector()
    #test_boxconnector_endurance(1)
    #test_scheduler()

    #test_event_publisher()

    print("Done")

