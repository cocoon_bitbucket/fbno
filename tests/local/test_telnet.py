"""


    local tests



    prerequisites:

        install restop wbackend

        be on the target livebox lan ( access via 192.168.1.1 )

        redis server running on localhost


"""

import time
import threading
import multiprocessing

from wbackend.model import Database,Model

from restop_queues.models import Queue,QueueHub
from restop_queues.controllers import QueueServerBase, ThreadedQueueServer, ProcessQueueServer

from restop_queues.adapters import GenericAdapter
from restop_queues.connectors import ConnectorFactory
from restop_queues.connectors.telnet_connector import TelnetConnector

from restop_queues.client import QueueClient


LATENCY=3

telnet_connection= dict(
    host= '192.168.1.1',
    port= 23,
    user= 'root',
    password= 'sah',
    user_prompt= 'login',
    password_prompt= 'assword'
)



def get_new_db():
    """

    :return:
    """
    db = Database(host='localhost', port=6379, db=0)
    db.flushall()
    Model.bind(database=db,namespace=None)
    return db


def get_queue():
    """


    :return:
    """

    name= 'telnet'
    parameters= telnet_connection

    q= Queue(name=name,parameters=parameters)
    q.save()

    return q


def get_adapter(run_mode='thread'):
    """

    :return:
    """

    queue= get_queue()

    adapter= GenericAdapter(queue,run_mode= run_mode)
    connnector_factory= ConnectorFactory( TelnetConnector,'telnet', **queue.parameters)
    adapter.bind_connector(connnector_factory)

    return adapter



def test_telnet_connector():
    """

    :return:
    """
    c= TelnetConnector('telnet',**telnet_connection)
    c.open()
    c.write('ls /tmp\n')
    time.sleep(1)
    #c.write('bye')
    line= c.readline()
    print(line)
    while line:
        line =c.readline()
        print(line)
    c.close()
    return



def test_queue_model():
    """

    :return:
    """
    db = get_new_db()

    name= 'telnet'
    parameters= telnet_connection

    q= Queue(name=name)
    q.save()

    q= Queue.load(name)
    assert q.name== name

    q.parameters= parameters
    q.save()
    assert q.parameters == parameters

    assert q.is_empty('stdin')

    q.write('some data','stdin')
    assert q.is_empty('stdin') == False
    assert q.size('stdin') == 1

    data= q.readline('stdin')
    assert data== 'some data'

    assert q.is_empty('stdin') == True
    assert q.size('stdin') == 0

    text= 'fill'
    for i in xrange(0,3):
        t= "fill%d" % (i+1)
        q.write(t,channel_name='stdin')
        q.write(t,channel_name='stdout')
        q.write(t, channel_name='stdlog')

    r= q.truncate(channel_name='stdlog',size=2)

    q.clear()
    assert q.size('stdin') == 0
    assert q.size('stdout') == 0
    assert q.size('stdlog') == 0


    return



def test_passive_adapter():
    """

    :return:
    """
    db = get_new_db()

    adapter= get_adapter(run_mode='passive')


    adapter.start()

    adapter.open_connector()

    adapter.write('echo "hello world\n"')

    adapter.run_once()
    adapter.run_once()

    line= adapter.readline()
    assert line != None

    adapter.close_connector()

    adapter.exit()


    return

def test_thread_adapter():
    """

    :return:
    """
    db = get_new_db()

    adapter= get_adapter(run_mode='thread')
    adapter.start()

    adapter.write("ls /tmp\n")

    time.sleep(LATENCY)

    line= adapter.readline()
    assert line != None

    time.sleep(4)
    #adapter.close_connector()

    adapter.exit()


    return





def test_client_expect():
    """

    :return:
    """
    db= get_new_db()
    queue= get_queue()

    server= get_adapter()
    server.start()

    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    client.write("hello there\n")

    text= "this what im looking for"
    client.write(text)

    client.write('dont care of this line')

    r= client.expect(text,timeout=120)

    assert 'found' in r[-1]

    client.exit()


    k = client.keep_alive()
    assert k == 'Done'





def test_client_sync():
    """

    :return:
    """
    db = get_new_db()
    queue = get_queue()

    adapter=get_adapter()
    adapter.start()


    agent_id= queue.get_hash_id()

    client= QueueClient(agent_id,redis_db=db)

    client.write("ls /tmp\n")


    r= client.sync(timeout=10)



    client.exit()


    k = client.keep_alive()
    assert k == 'Done'




def test_mock_concern():
    """


    :return:
    """

    db = get_new_db()

    q= get_queue()

    agent_id = q.get_hash_id()
    client = QueueClient(agent_id, redis_db=db)
    mock= QueueClient(agent_id, redis_db=db)

    server= QueueServerBase(q)

    # do not start
    server._mode= None
    server.start()
    assert server.is_alive() == False

    text= 'some data'
    # client write some text  ( stdin implicit )
    client.write(text)

    # mock readline it from stdin explicitly
    r= mock.readline(channel_name='stdin')

    # mock transform it and respond
    mock.write(text.upper(), channel_name='stdout')

    # client readline it
    #time.sleep(1)
    line= client.readline()

    assert line == text.upper()

    client.exit()


    return




if __name__=="__main__":


    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_telnet_connector()
    test_queue_model()

    test_passive_adapter()
    test_thread_adapter()



    #test_client_expect()


    test_client_sync()

    test_mock_concern()



    print "Done"