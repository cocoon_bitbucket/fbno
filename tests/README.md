setup virtualenv v-fbno for testing


    cd <fbno>
    source v-fbno/bin/activate
    mkdir tmp
    cd tmp
    
install wbackend for local tests
--------------------------------
    
    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/wbackend restop
    cd restop
    pip install -r requirements.txt
    python setup.py install
    
    
install restop-client for client tests
---------------------------------------

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/restop-client restop-client
    cd restop-client
    pip install -r requirements.txt
    python setup.py install
    